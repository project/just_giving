<?php

namespace Drupal\just_giving;

/**
 * Interface JustGivingCountriesInterface.
 */
interface JustGivingCountriesInterface {

  public function getCountriesFormList();

}
